public with sharing class RandomUtils {
    public static Integer getRandomPositiveInteger(Integer max)
    {
        return Integer.valueOf(Math.random() * max);
    }

    public static sObject getRandomElement(List<SObject> objs)
    {
        return objs[getRandomPositiveInteger(objs.size() - 1)];
    }
}
