public with sharing class genericChildController {

    // Returns child objects that will be selectable in createGenericChildLWC component's picklist
    // Only returns child objects supported by the UI API and that the user can create
    @AuraEnabled(cacheable=true)
    public static List<String> getChildObjectNames(Id recordId)
    {
        List<String> childrenNames = new List<String>();

        // Get ChildRelationships
        SObjectType recordType = recordId.getSobjectType();
        List<Schema.ChildRelationship> childRelationships = recordType.getDescribe().getChildRelationships();

        for(Schema.ChildRelationship eachChild : childRelationships) {
            Boolean isCustom = eachChild.getChildSObject().getDescribe().isCustom();
            Boolean isSupportedByUiApi = Constants.UI_API_COMPATIBLE_OBJECTS.contains('' + eachChild.getChildSObject());
            Boolean userCanCreate = eachChild.getChildSObject().getDescribe().isCreateable();
            Boolean userCanAccess = eachChild.getChildSObject().getDescribe().isAccessible();

            if((isCustom || isSupportedByUiApi) && userCanCreate && userCanAccess) {
                // Pull out API name
                childrenNames.add('' + eachChild.getChildSObject()); 
            }
        }
        return childrenNames;
    }

    // Returns fields that user will enter when creating a record on createGenericChildForm component
    @AuraEnabled(cacheable=true)
    public static List<String> getMandatoryFields(String apiName) {
        List<String> result = new List<String>();
        Set<String> mandatoryFields = new Set<String>();

        mandatoryFields.addAll(getConfiguredFields(apiName));
        mandatoryFields.addAll(getRequiredObjectFields(apiName));
        result.addAll(mandatoryFields);
        return result;
    }

    // Returns fields on the object marked as Required
    @testVisible
    private static List<String> getRequiredObjectFields(String apiName)
    {
        List<String> requiredFields = new List<String>();

        // Map fields
        SObjectType recordType = ((SObject)(Type.forName('Schema.'+apiName).newInstance())).getSObjectType(); // See: https://salesforce.stackexchange.com/questions/218982/why-is-schema-describesobjectstypes-slower-than-schema-getglobaldescribe
        Map<String,Schema.SObjectField> fieldMap = recordType.getDescribe().fields.getMap();

        // Find required, custom fields
        for (Schema.SObjectField eachField : fieldMap.values())
        {
            Schema.DescribeFieldResult field = eachField.getDescribe();
            // Checking if field is Required: https://salesforce.stackexchange.com/questions/260294/in-order-to-check-if-a-field-is-required-or-not-is-the-result-of-isnillable-met
            if (field.isCreateable() && !field.isNillable() && !field.isDefaultedOnCreate()) {
                requiredFields.add('' + eachField);
            }
        }

        // Return API names
        return requiredFields;
    }

    // Returns fields marked as mandatory in the object's Mandatory Metadata Mapping metadata record
    @testVisible
    private static List<String> getConfiguredFields(String apiName){
        String query = 'SELECT Object_Name__c, Field_List__c FROM Mandatory_Field_Mapping__mdt WHERE Object_Name__c = :apiName';
        Mandatory_Field_Mapping__mdt[] mandatoryMapping = Database.query(query);

        if(mandatoryMapping.size() > 0) {
            String fields = mandatoryMapping[0].Field_List__c.deleteWhitespace();
            return fields.split(',');
        }
        return new List<String>();
    }

    // Returns the relationship name between a passed child and parent (if it exists). Ex: Contact,Account => AccountId
    @AuraEnabled(cacheable=true)
    public static String getParentRelationshipName(String childApiName, String parentApiName)
    {
        SObjectType recordType = ((SObject)(Type.forName('Schema.'+childApiName).newInstance())).getSObjectType();

        // Walk through fields
        for(Schema.SobjectField strFld: recordType.getDescribe().fields.getMap().Values()) {
            // If it's a parent reference field...
            if(strFld.getDescribe().getType() == Schema.DisplayType.REFERENCE) {
                // Find parent's relationship and then return its Name (ex: AccountId)
                if('' + strFld.getDescribe().getReferenceTo()[0] == parentApiName) {
                    return strFld.getDescribe().getName();
                }
            } 
        }
        return '';
    }
    
}
