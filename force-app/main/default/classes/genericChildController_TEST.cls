@isTest
private class genericChildController_TEST {


    // ** Update this if you update the configured fields for Account **
    private static final String ACCOUNT_REQUIRED_FIELDS = 'Name';
    private static final String ACCOUNT_CONFIGURED_FIELDS = 'Description,Phone';

    private static final String TEST_ACC_NAME = 'testAcc';
    private static final String TEST_ATCH_NAME = 'testAtch';

    @TestSetup
    static void makeData(){
        Account someAcc = new Account(Name=TEST_ACC_NAME);
        insert someAcc;

        Attachment someAtch = new Attachment(Name='testAtch');
        someAtch.parentId = someAcc.Id;
        someAtch.body = Blob.valueOf('bodyhere');
        insert someAtch;
    }
    
    @isTest
    static void doesGetChildObjectNamesGetValidChildren()
    {
        List<String> expectedList = new List<String>{'Account', 'Contact', 'Case'};
        Account someAcc = [SELECT Id FROM Account WHERE Name=:TEST_ACC_NAME LIMIT 1];

        Test.startTest();
        List<String> actualList = genericChildController.getChildObjectNames(someAcc.Id);
        Test.stopTest();

        for(String eachExpected : expectedList) {
            System.assert(actualList.contains(eachExpected));
        }
    }

    @isTest
    static void doesGetChildObjectNamesReturnEmptyWithNoValidChildren()
    {
        Account someAcc = [SELECT Id FROM Account WHERE Name=:TEST_ACC_NAME LIMIT 1];
        Attachment someAtch = [SELECT Id FROM Attachment WHERE Name=:TEST_ATCH_NAME LIMIT 1]; // No valid children

        Test.startTest();
        List<String> actualList = genericChildController.getChildObjectNames(someAtch.Id);
        Test.stopTest();

        System.assertEquals(0, actualList.size());
    }

    @isTest
    static void doesGetConfiguredFieldsReturnProperFields()
    {
        List<String> expectedList = ACCOUNT_CONFIGURED_FIELDS.split(',');
        String apiName = 'Account';
        
        test.startTest();
        List<String> actualList = genericChildController.getConfiguredFields(apiName);
        test.stopTest();

        for(String eachExpected : expectedList) {
            System.assert(actualList.contains(eachExpected));
        }
        System.assertEquals(expectedList.size(), actualList.size());
    }

    @isTest
    static void doesGetRequiredObjectFieldsReturnProperFields()
    {
        List<String> expectedList = ACCOUNT_REQUIRED_FIELDS.split(',');
        String apiName = 'Account';
        
        test.startTest();
        List<String> actualList = genericChildController.getRequiredObjectFields(apiName);
        test.stopTest();

        for(String eachExpected : expectedList) {
            System.assert(actualList.contains(eachExpected));
        }
        System.assertEquals(expectedList.size(), actualList.size());
    }

    @isTest
    static void doesGetParentRelationshipNameReturnCorretName()
    {
        String contactApiName = 'Contact';
        String accountApiName = 'Account';

        String expected_1 = 'AccountId';
        String expected_2 = 'MasterRecordId';
        String expected_3 = '';

        Test.startTest();
        // Actual definition: getParentRelationshipName(child, parent)
        String actual_1 = genericChildController.getParentRelationshipName(contactApiName, accountApiName);
        String actual_2 = genericChildController.getParentRelationshipName(accountApiName, accountApiName);
        String actual_3 = genericChildController.getParentRelationshipName(accountApiName, contactApiName);
        Test.stopTest();

        System.assertEquals(expected_1, actual_1);
        System.assertEquals(expected_2, actual_2);
        System.assertEquals(expected_3, actual_3);
    }

    @isTest
    static void doesGetMandatoryFieldsReturnBothConfiguredAndRequiredFields()
    {
        List<String> expectedList = ACCOUNT_REQUIRED_FIELDS.split(',');
        expectedList.addAll(ACCOUNT_CONFIGURED_FIELDS.split(','));
        String apiName = 'Account';

        Test.startTest();
        List<String> actualList = genericChildController.getMandatoryFields(apiName);
        Test.stopTest();

        for(String eachExpected : expectedList) {
            System.assert(actualList.contains(eachExpected));
        }
        System.assertEquals(expectedList.size(), actualList.size());
    }
}
