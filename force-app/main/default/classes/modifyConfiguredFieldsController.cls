public with sharing class modifyConfiguredFieldsController {
    
    // Returns list of Org's SObjects
    @AuraEnabled(cacheable=true)
    public static List<String> getSobjectNames()
    {
        Map<String, SObjectType> objs = Schema.getGlobalDescribe();
        List<String> result = new List<String>(objs.keySet());
        result.sort();
        return result;
    }

    // Returns list of fields for passed Object
    @AuraEnabled(cacheable=true)
    public static List<String> getObjectFields(String apiName)
    {
        List<String> fields = new List<String>();

        SObjectType recordType = ((SObject)(Type.forName('Schema.'+apiName).newInstance())).getSObjectType();
        Map<String,Schema.SObjectField> fieldMap = recordType.getDescribe().fields.getMap();

        for (Schema.SObjectField eachField : fieldMap.values()) {
            Schema.DescribeFieldResult field = eachField.getDescribe();
            fields.add('' + eachField);
        }
        fields.sort();
        return fields;
    }

    // Adds/Modifies the Mandatory Field Mapping metadata record
    // Object_Name__c = objectName, Field_List__c = fieldList
    @AuraEnabled
    public static void updateMetadata(String objectName, String fieldList)
    {
        // Create the metadata type
        Metadata.CustomMetadata mandatoryFieldsRecord = new Metadata.CustomMetadata();
        mandatoryFieldsRecord.fullName = 'Mandatory_Field_Mapping.' + objectName;
        mandatoryFieldsRecord.label = objectName;
        
        // Set field values
        Metadata.CustomMetadataValue objectNameField = new Metadata.CustomMetadataValue();
        objectNameField.field = 'Object_Name__c';
        objectNameField.value = objectName;
        Metadata.CustomMetadataValue fieldListField = new Metadata.CustomMetadataValue();
        fieldListField.field = 'Field_List__c';
        fieldListField.value = fieldList;
        // Add field values
        mandatoryFieldsRecord.values.add(objectNameField);
        mandatoryFieldsRecord.values.add(fieldListField);
        
        // Deploy changes
        Metadata.DeployContainer mdContainer = new Metadata.DeployContainer();
        mdContainer.addMetadata(mandatoryFieldsRecord);
        CustomMetadataCallback callback = new CustomMetadataCallback();
        Id jobId = Metadata.Operations.enqueueDeployment(mdContainer, callback);
    }
}
