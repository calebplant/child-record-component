@isTest
private without sharing class modifyConfiguredFieldsController_TEST {
    
    @isTest
    static void doesGetSobjectNamesReturnListOfAllNames()
    {
        Map<String, SObjectType> expectedObjMap = Schema.getGlobalDescribe();
        Set<String> expectedSet = new Set<String>(expectedObjMap.keySet());

        Test.startTest();
        List<String> actualList = modifyConfiguredFieldsController.getSobjectNames();
        Test.stopTest();

        System.assert(expectedSet.containsAll(actualList));
    }

    @isTest
    static void doesGetObjectFieldsReturnListOfAllFields()
    {
        String apiName = 'Account';
        Map<String,Schema.SObjectField> expectedFieldMap = Account.getSObjectType().getDescribe().fields.getMap();
        Set<String> expectedSet = new Set<String>(expectedFieldMap.keySet());

        Test.startTest();
        List<String> actualList = modifyConfiguredFieldsController.getObjectFields(apiName);
        List<String> result = new List<String>();
        for(String eachActual : actualList) {
            result.add(eachActual.toLowerCase());
        }
        Test.stopTest();

        System.assert(expectedSet.containsAll(result));
    }

    @isTest
    static void doesCustomMetadataCallbackHandleResult()
    {
        Metadata.DeployCallback callback = new CustomMetadataCallback();
        Metadata.DeployResult result = new Metadata.DeployResult();
        result.numberComponentErrors = 1;
        Metadata.DeployCallbackContext context = new Metadata.DeployCallbackContext();
        callback.handleResult(result, context);
    }

    @isTest
    static void doesUpdateMetadataDeployChanges()
    {
        //TODO: https://developer.salesforce.com/blogs/engineering/2015/05/testing-custom-metadata-types.html
    }
}
