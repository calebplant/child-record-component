({
    doInit : function(component, event, helper) {
        console.log('Initializing aura component . . .');
        console.log(component.get("v.sObjectName"));
        console.log(component.get("v.recordId"));
    },

    handleClose : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },

    handleSuccess : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "The record has been created successfully.",
            "type": "success"
        });
        toastEvent.fire();
    }
})
