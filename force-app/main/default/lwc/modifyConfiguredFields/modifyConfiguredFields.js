import { LightningElement, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import getSobjectNames from '@salesforce/apex/modifyConfiguredFieldsController.getSobjectNames';
import getObjectFields from '@salesforce/apex/modifyConfiguredFieldsController.getObjectFields';
import updateMetadata from '@salesforce/apex/modifyConfiguredFieldsController.updateMetadata';

export default class ModifyConfiguredFields extends LightningElement {

    objectNames;
    fieldNames;
    error;
    selectedObjectType;
    selectedFields;

    @wire(getSobjectNames)
    getObjectNames(response) {
        if(response.data) {
            this.objectNames = response.data.map(eachObj => {
                return {label: eachObj, value: eachObj};
            });
            console.log('Retrieved object names: ');
            console.log(this.objectNames);
        }
        if(response.error) {
            this.error = response.error;
            console.log('Error retrieving object names!');
            console.log(response.error);    
        }
    }
    
    @wire(getObjectFields, {apiName: '$selectedObjectType'})
    getFieldNames(response) {
        if(response.data) {
            this.fieldNames = response.data.map(eachField => {
                return {label: eachField, value: eachField};
            });
            console.log('Retrieved field names: ');
            console.log(this.fieldNames);
        }
        if(response.error) {
            this.error = response.error;
            console.log('Error retrieving field names!');
            console.log(response.error);    
        }
    }

    get cardTitle() {
        return this.selectedObjectType ? 'Set Mandatory Fields for ' + this.selectedObjectType : 'Set Mandatory Fields for Object';
    }

    // Event handlers
    handleObjectSelected(event)
    {
        console.log('modifyConfiguredFields :: handleObjectSelected');
        
        this.selectedObjectType = event.detail.value;
        console.log('Selected Object Type: ' + this.selectedObjectType);
    }

    handleChange(event)
    {
        this.selectedFields = event.detail.value;
    }

    handleSaveChanges()
    {
        updateMetadata({objectName: this.selectedObjectType, fieldList: this.selectedFields.join()})
            .then(result => {
                console.log('Successfully called updateMetadata()');

                const event = new ShowToastEvent({
                    title: 'Success',
                    variant: 'success',
                    message: 'Successfully enqueued changes. They should be done almost immediately.',
                });
                this.dispatchEvent(event);
            })
            .catch(error => {
                console.log('Error calling updateMetadata()!');
                console.log(error);

                const event = new ShowToastEvent({
                    title: 'Failure',
                    variant: 'error',
                    message: 'There was an issue deploying your changes. Please try again or contact your local, friendly developer.',
                });
                this.dispatchEvent(event);
            })
    }
}