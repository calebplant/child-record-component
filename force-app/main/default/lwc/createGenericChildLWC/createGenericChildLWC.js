import { LightningElement, wire, api } from 'lwc';
import getChildObjectNames from '@salesforce/apex/genericChildController.getChildObjectNames';
import { NavigationMixin } from 'lightning/navigation';

export default class CreateGenericChildLWC extends NavigationMixin(LightningElement) {
    // Public
    @api recordId;
    @api recordApiName;

    // Private
    error;
    childApiNames;
    selectedChildType = '';
    _recordResponse;

    @wire(getChildObjectNames, {recordId: '$recordId'})
    getChildrenApiNames(response)
    {
        // console.log('createGenericChildLWC :: getChildrenApiNames');
        console.log(response.data);
        
        this._recordResponse = response;
        if(response.data) {
            let uniqueData = [...new Set(response.data)];
            this.childApiNames = uniqueData.map(eachChild => {
                return {label: eachChild, value: eachChild};
            });
            // console.log('Child api names:');
            // console.log(this.childApiNames);
        }
        if(response.error) {
            // console.log('Error fetching child API names!');
            // console.log(response.error);
            this.error = response.error;
        }
    }

    get childIsSelected() {
        return this.selectedChildType === '' ? false : true;
    }

    get cardTitle() {
        return this.selectedChildType !== '' ? 'Create New ' + this.selectedChildType : 'Create New Child';
    }

    // Event Handlers
    handleChange(event) {
        // console.log('createGenericChildLWC :: handleChange');
        this.selectedChildType = event.detail.value;
        // console.log('Selected Child Type: ' + this.selectedChildType);
    }

    handleClose(event) {
        // console.log('createGenericChildLWC :: handleClose');

        const closeEvent = new CustomEvent('close');
        this.dispatchEvent(closeEvent);
    }

    handleSuccess(event) {
        // console.log('createGenericChildLWC :: handleSuccess');
        // console.log('New record Id: ' + event.detail);
        
        const successEvent = new CustomEvent('success', {detail: event.detail});
        this.dispatchEvent(successEvent);
    }

    handleEditFields(event) {
        this[NavigationMixin.Navigate]({
            type: 'standard__navItemPage',
            attributes: {
                apiName: 'Configure_Fields'
            }
        });
    }
}