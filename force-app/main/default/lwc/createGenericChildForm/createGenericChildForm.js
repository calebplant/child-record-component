import { LightningElement, api, wire } from 'lwc';
import getMandatoryFields from '@salesforce/apex/genericChildController.getMandatoryFields';
import getParentRelationshipName from '@salesforce/apex/genericChildController.getParentRelationshipName';

export default class CreateGenericChildForm extends LightningElement {
    @api objectApiName;
    @api parentApiName;
    @api parentId;

    requiredFields;
    parentRelationshipName;
    _recordResponse;

    @wire(getMandatoryFields, {apiName: '$objectApiName'})
    getRequiredFields(response) {
        // console.log('createGenericChildLWC :: getMandatoryFields');
        // console.log('apiName: ' + this.objectApiName);
        // console.log('Returned data: ');
        // console.log(response.data);
        
        this._recordResponse = response;
        if(response.data) {
            // Find field name for child -> parent relationship
            getParentRelationshipName({childApiName: this.objectApiName, parentApiName: this.parentApiName})
            .then(result => {
                this.parentRelationshipName = result;
                // Populate form fields
                this.requiredFields = response.data.map(eachField => {
                    if(eachField != this.parentRelationshipName) { // Prevent duplicate parent lookup
                        return {name: eachField, value: null};
                    }
                });
                // Add lookup parent field with prepopulated value
                this.requiredFields = this.requiredFields.concat([{name: result, value: this.parentId}]);
                // Filter any potential falsy values
                this.requiredFields = this.requiredFields.filter(Boolean); 
            }).catch(error => {
                console.log(error);
                this.error = error;
            });
        }
        if(response.error) {
            // console.log('Error fetching mandatory fields!');
            // console.log(response.error);
            this.error = response.error;
        }
    }

    /*
    @wire(getParentRelationshipName, {childApiName: '$objectApiName', parentApiName: '$parentApiName'})
    findParentRelationshipName(response)
    {
        // console.log('createGenericChildLWC :: getParentRelationshipName');
        // console.log('childApiName: ' + this.objectApiName);
        // console.log('parentApiName: ' + this.parentApiName);
        
        this._recordResponse = response;
        if(response.data) {
            // console.log('Returned data: ');
            // console.log(response.data);
            this.parentRelationshipName = response.data;
        }
        if(response.error) {
            // console.log('Error finding parent relationship name!');
            // console.log(response.error);
            this.error = response.error;
        }
    }
    */

    // Event handlers
    handleSuccess(event) {
        // console.log('createGenericChildForm :: handleSuccess');
        // console.log('New record Id: ' + event.detail.id);
        
        const closeEvent = new CustomEvent('success', {detail: event.detail.id});
        this.dispatchEvent(closeEvent);

        this.handleClose();
    }

    handleClose() {
        const closeEvent = new CustomEvent('close');
        this.dispatchEvent(closeEvent);
    }
}