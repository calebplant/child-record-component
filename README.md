# Generic Child Creation Component

## Quick Pictures

The below example uses the quick action on an Account detail page to create a new Opportunity record related ot the Account.

![quick_action.png](media/quick_action.png)

![component_popup_1.png](media/component_popup_1.png)

![component_popup_2.png](media/component_popup_2.png)

Note that the fields displayed are those required to create the record AND those configured to display on the object's Mandatory Field Mapping metadata record. For Opportunity, see:

![custom_metadata_opp.png](media/custom_metadata_opp.png)

## Quick Links

### Child Creation Component

This is the actual component that pops up when the user click the Quick Action.

#### Aura

* [createGenericChildForm html](force-app/main/default/aura/createGenericChild/createGenericChild.cmp)
* [createGenericChildFormController](force-app/main/default/aura/createGenericChild/createGenericChildController.js)

#### LWC

* [Base LWC html](force-app/main/default/lwc/createGenericChildLWC/createGenericChildLWC.html)
* [Base LWC Javascript](force-app/main/default/lwc/createGenericChildLWC/createGenericChildLWC.js)
* [Child Form LWC html](force-app/main/default/lwc/createGenericChildForm/createGenericChildForm.html)
* [Child Form LWC Javascript](force-app/main/default/lwc/createGenericChildForm/createGenericChildForm.js)

#### Apex

* [genericChildController](force-app/main/default/classes/genericChildController.cls)

### Modifying Custom Metadata

This is a component that lives on its own tab and is used for easily editing Mandatory Field Mapping metadata records

![edit_metadata_component.png](media/edit_metadata_component.png)

#### LWC

* [modifyConfiguredFields html](force-app/main/default/lwc/modifyConfiguredFields/modifyConfiguredFields.html)
* [modifyConfiguredFields Javascript](force-app/main/default/lwc/modifyConfiguredFields/modifyConfiguredFields.js)

#### Apex

* [modifyConfiguredFieldsController](force-app/main/default/classes/modifyConfiguredFieldsController.cls)
* [CustomMetadataCallback](force-app/main/default/classes/CustomMetadataCallback.cls)

### Tests

* [genericChildController_TEST](force-app/main/default/classes/genericChildController_TEST.cls)
* [modifyConfiguredFieldsController_TEST](force-app/main/default/classes/modifyConfiguredFieldsController_TEST.cls)

### Custom Metadata Types

* [Mandatory_Field_Mapping__mdt](force-app/main/default/objects/Mandatory_Field_Mapping__mdt)

### Static Resources

* [Supported_UI_Objects.resource-meta.xml](force-app/main/default/staticresources/Supported_UI_Objects.resource-meta.xml)

### Tabs
* [Configure_Fields.tab-meta.xml](force-app/main/default/tabs/Configure_Fields.tab-meta.xml)